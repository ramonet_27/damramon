import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;

import net.xqj.basex.BaseXXQDataSource;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.cmd.CreateDB;


public class Exercici1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BaseXServer server;
		final int cops=20;
		long total1=0, total2=0, total3=0;
		try {
			server = new BaseXServer();
			new CreateDB("fb", "/home/ausias/Escriptori/DADES/factbook.xml").execute(server.context);
		} catch (IOException e1) {
			e1.printStackTrace();
		} 
		
		Scanner sc = new PersonalScanner().getInstance();
		List<String> grups=new ArrayList<String>();
		ClientSession cs;
		XQDataSource xqs;
		String nom="Turkey";
		xqs = new BaseXXQDataSource();
		XQConnection conn = null;
		String pregunta;
		XQPreparedExpression preP;
		XQResultSequence res;
		Pais p;
		try {
			xqs.setProperty("serverName", "localhost");
			xqs.setProperty("port", "1984");
			xqs.setProperty("databaseName", "fb");
			xqs.setProperty("user", "admin");
			xqs.setProperty("password", "admin");
			conn = xqs.getConnection();
		} catch (XQException e) {
			e.printStackTrace();
		}
		try {
			cs = new ClientSession("localhost",1984,"admin","admin");
			////////////////1////////////////////7
			for(int i=0;i<cops;i++){ 
				long temps = System.currentTimeMillis();
				 
				pregunta = "declare variable $nomP as xs:string external; for $c in //country where $c/name = $nomP return $c/ethnicgroups/text()";
				preP = conn.prepareExpression(pregunta);
				preP.bindString(new QName("nomP"), nom, null);
				res = preP.executeQuery();
				while(res.next()) grups.add(res.getItemAsString(null));
				
				pregunta = "declare variable $nomP as xs:string external; for $c in //country where $c/name = $nomP return sum($c/border/@length/data())";
				preP = conn.prepareExpression(pregunta);
				preP.bindString(new QName("nomP"), nom, null);
				res = preP.executeQuery();
				res.next();
				
				p=new Pais("Turkey", Double.parseDouble(res.getItemAsString(null)), grups);
				System.out.println(p.toString()+ " temps: "+(System.currentTimeMillis()-temps)+" milisegons");

				temps = System.currentTimeMillis()-temps;
				total1+=temps;
			}
			
			////////////////////////////////2 ////////////////////////////////
				for(int i=0;i<cops;i++){
					long temps = System.currentTimeMillis();
					float borders = 0;
					
					pregunta = "declare variable $nomP as xs:string external; for $c in //country where $c/name = $nomP return ($c/ethnicgroups, $c/border)";
					preP = conn.prepareExpression(pregunta);
					preP.bindString(new QName("nomP"), nom, null);
					res = preP.executeQuery();
		
					borders = 0;
					grups=new ArrayList<String>();
					while(res.next()){
						try {
							XMLStreamReader reader = res.getItemAsStream();
							while(reader.hasNext()){
								int tipusEvent = reader.next();
								switch(tipusEvent){
									case XMLStreamReader.START_ELEMENT:
										if(reader.getLocalName().equals("ethnicgroups")){
											grups.add(reader.getElementText());
										} 
										else if(reader.getLocalName().equals("border")){
											borders += Double.parseDouble(reader.getAttributeValue(1));
										}
									break; 
									
									default:							
											break;
								}
							}
							p=new Pais("Turkey", borders, grups);
							System.out.println(p.toString());
						} catch (XMLStreamException e) {
							e.printStackTrace();
						}
					}
					temps = System.currentTimeMillis()-temps; 
					total2+=temps;
				}
				
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
				
				/////////////////////3/////////////////////////
				for(int i=0;i<cops;i++){
					long temps = System.currentTimeMillis();
					float borders = 0;
					
					pregunta = "for $c in //country return ($c/name, $c/ethnicgroups, $c/border)";
					preP = conn.prepareExpression(pregunta);
					res = preP.executeQuery();
		
					
					boolean trobat = false;
					borders = 0; 
					grups=new ArrayList<String>();
					while(res.next()){ 
						try {
							XMLStreamReader reader = res.getItemAsStream();
							while(reader.hasNext()){
								int tipusEvent = reader.next();
								switch(tipusEvent){
									case XMLStreamReader.START_ELEMENT:
										if(reader.getLocalName().equals("name")){
											String pais=reader.getElementText().trim();
											if(pais.equals(nom)){
												trobat = true;
											} else trobat = false;
										}  

										if(trobat){
											if(reader.getLocalName().equals("ethnicgroups") ){
												grups.add(reader.getElementText());
											} 
											else if(reader.getLocalName().equals("border")){
												borders += Double.parseDouble(reader.getAttributeValue(1));
											}
										}
									break; 
									
									default:							
											break;
								}
							} 

						} catch (XMLStreamException e) {
							e.printStackTrace();
						}
					} 
					p=new Pais("Turkey", borders, grups);
					System.out.println(p.toString());

					temps = System.currentTimeMillis()-temps; 
					total3+=temps;
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XQException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		total1/=cops;
		total2/=cops;
		total3/=cops; 
		System.out.println("el 1 tarda "+total1+" milisegons.");
		System.out.println("el 2 tarda "+total2+" milisegons.");
		System.out.println("el 3 tarda "+total3+" milisegons.");
	}

}
