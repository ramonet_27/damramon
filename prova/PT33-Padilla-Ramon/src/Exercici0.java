import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;

import net.xqj.basex.BaseXXQDataSource;

import org.basex.BaseXServer;
import org.basex.core.cmd.Close;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.XQuery;


public class Exercici0 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//pel test tenim en compte la lectura de les dades un cop obtingudes.
		final int cops=50;
		try {
			BaseXServer server = new BaseXServer();
			new CreateDB("fb", "/home/ausias/Escriptori/DADES/factbook.xml").execute(server.context);
			String res;
			long temps1=0, temps2=0, total1=0, total2=0;
			for(int i=0;i<cops;i++)
			{
				temps1 = System.currentTimeMillis();
				res = new XQuery("for $c in //country return $c/name/text()").execute(server.context);
				temps1 = System.currentTimeMillis()-temps1;
				total1+=temps1;
				System.out.println(res);
			}
			System.out.println(".......");
			XQDataSource xqs; 
			xqs = new BaseXXQDataSource();
			
				try {
					xqs.setProperty("serverName", "localhost");
					xqs.setProperty("port", "1984");
					xqs.setProperty("databaseName", "fb");
					xqs.setProperty("user", "admin");
					xqs.setProperty("password", "admin");
					XQConnection conn = xqs.getConnection();
					XQPreparedExpression preP;
					XQResultSequence res2;
					
					for(int i=0;i<cops;i++)
					{
						temps2 = System.currentTimeMillis();
						preP = conn.prepareExpression("for $c in //country return $c/name/text()");
						res2 = preP.executeQuery();
						temps2 = System.currentTimeMillis()-temps2;
						total2+=temps2;
						while(res2.next()) System.out.println(res2.getItemAsString(null));
					}
					total1/=cops;
					total2/=cops;
					System.out.println("BaseX "+total1+" milisegons");
					System.out.println("XQJ "+total2+" milisegons");
					if(total2>total1) 
						System.out.println("XQJ ha tardat "+(total2/total1)+" cops més." );
					else if(total2<total1)
						System.out.println("BaseX ha tardat "+(total1/total2)+" cops més." );
					else 
						System.out.println("empat!");
				} catch (XQException e) {
					e.printStackTrace();
				}
				
			new Close().execute(server.context);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
