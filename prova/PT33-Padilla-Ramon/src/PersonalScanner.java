import java.util.Scanner;

//::.. ENS ASSEGUREM D'OBRIR UNA SOLA INSTÀNCIA DEL SCANNER ..:://
public class PersonalScanner {
	private static Scanner scan = null;
	
	public static  Scanner getInstance(){
		if(scan == null)
			scan = new Scanner(System.in);
		return scan;
	}
}
