import java.util.List;


public class Pais {
	String nom;
	double longitudFronteres;
	List <String> grupsEtnics;
	public Pais(String nom, double longitudFronteres, List<String> grupsEtnics) {
		super();
		this.nom = nom;
		this.longitudFronteres = longitudFronteres;
		this.grupsEtnics = grupsEtnics;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public double getLongitudFronteres() {
		return longitudFronteres;
	}
	public void setLongitudFronteres(int longitudFronteres) {
		this.longitudFronteres = longitudFronteres;
	}
	public List<String> getGrupsEtnics() {
		return grupsEtnics;
	}
	public void setGrupsEtnics(List<String> grupsEtnics) {
		this.grupsEtnics = grupsEtnics;
	}
	@Override
	public String toString() {
		return "Pais [nom=" + nom + ", longitudFronteres=" + longitudFronteres
				+ ", grupsEtnics=" + grupsEtnics + "]";
	}
	
}
