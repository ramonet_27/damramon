import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;

import net.xqj.basex.BaseXXQDataSource;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.BaseXException;
import org.basex.core.cmd.Close;
import org.basex.core.cmd.CreateDB;


public class Exercici3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BaseXServer server = null;	//he retallat la llista de paissos per fer-la més curta.
		long total1=0, total2=0, total3=0, quatre1=0,quatre2=0,quatre3=0;
		List<String> paissos = new ArrayList<String>();
		Scanner sc = new PersonalScanner().getInstance();
		try { 
			server = new BaseXServer();
			new CreateDB("fb", "/home/ausias/Escriptori/DADES/factbook.xml").execute(server.context);
			try {
				Thread.sleep(5000); 
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		BufferedReader br = null;
		ClientSession cs;
		XQDataSource xqs;
		String nom;
		xqs = new BaseXXQDataSource();
		XQConnection conn = null;
		String pregunta;
		XQPreparedExpression preP;
		XQResultSequence res;
		Pais p;
		try {
			xqs.setProperty("serverName", "localhost");
			xqs.setProperty("port", "1984");
			xqs.setProperty("databaseName", "fb");
			xqs.setProperty("user", "admin");
			xqs.setProperty("password", "admin");
			conn = xqs.getConnection();
		} catch (XQException e) {
			e.printStackTrace();
		}
		try {
			cs = new ClientSession("localhost",1984,"admin","admin");
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			br = new BufferedReader(new FileReader("llistapaissos.txt"));
			 try {
			        StringBuilder sb = new StringBuilder();
			        String line = br.readLine();

			        while (line != null) {
			            sb.append(line);
			            sb.append(System.lineSeparator());
			            line = br.readLine();
			            nom = line;
			            paissos.add(line);
			            if(line == null) break;
			      
		List<String> grups=new ArrayList<String>();
		
		try {
			
			////////////////1////////////////////7
			
				long temps = System.currentTimeMillis(); 
				
				
					
					pregunta = "declare variable $nomP as xs:string external; for $c in //country where $c/name = $nomP return $c/ethnicgroups/text()";
					preP = conn.prepareExpression(pregunta);
					for(String pai: paissos){
						preP.bindString(new QName("nomP"), pai, null);
						res = preP.executeQuery();
						while(res.next()) grups.add(res.getItemAsString(null));
						
						pregunta = "declare variable $nomP as xs:string external; for $c in //country where $c/name = $nomP return sum($c/border/@length/data())";
						preP = conn.prepareExpression(pregunta);
						preP.bindString(new QName("nomP"), nom, null);
						res = preP.executeQuery();
						res.next();
						
						p=new Pais(pai, Double.parseDouble(res.getItemAsString(null)), grups);
						//System.out.println("part 1" + p.toString());
					}
					
	 
					temps = System.currentTimeMillis()-temps;
					total1+=temps;
					if(paissos.size() == 4) quatre1=total1;
					
				
				
				////////////////////////////////2 ////////////////////////////////
					
						temps = System.currentTimeMillis();
						float borders = 0;
						
						pregunta = "declare variable $nomP as xs:string external; for $c in //country where $c/name = $nomP return ($c/ethnicgroups, $c/border)";
						preP = conn.prepareExpression(pregunta);
						for(String pai: paissos){
							preP.bindString(new QName("nomP"), pai, null);
							res = preP.executeQuery();
							borders = 0; 
							grups=new ArrayList<String>();
							while(res.next()){
								try {
									XMLStreamReader reader = res.getItemAsStream();
									while(reader.hasNext()){
										int tipusEvent = reader.next();
										switch(tipusEvent){
											case XMLStreamReader.START_ELEMENT:
												if(reader.getLocalName().equals("ethnicgroups")){
													grups.add(reader.getElementText());
												} 
												else if(reader.getLocalName().equals("border")){
													borders += Double.parseDouble(reader.getAttributeValue(1));
												}
											break; 
											
											default:							
													break;
										}
									}
									
								} catch (XMLStreamException e) {
									e.printStackTrace();
								}
							} 
							p=new Pais(pai, borders, grups); 
							//System.out.println("part 2" + p.toString());
						}
						
			
						
						temps = System.currentTimeMillis()-temps; 
						total2+=temps;
						if(paissos.size() == 4) quatre2=total2;
					
				
					
					/////////////////////3/////////////////////////
					
						temps = System.currentTimeMillis();
						borders = 0;
						
						pregunta = "for $c in //mondial return $c";
						
			
						
						boolean trobat = false, paisTrobat = false;
						
						System.out.println("|||||||||||||||||||||||||||||||||");
					for(String pa: paissos){
						borders = 0; 
						grups=new ArrayList<String>();
						trobat = false;
						paisTrobat = false;
						preP = conn.prepareExpression(pregunta);
						res = preP.executeQuery();
						String pai="";
						System.out.println("---------------"+pa+"--------------");
						
						while(res.next()){  
							try { 
								XMLStreamReader reader = res.getItemAsStream();
								while(reader.hasNext()){
					 				int tipusEvent = reader.next();
									switch(tipusEvent){
										case XMLStreamReader.START_ELEMENT:
											if(reader.getLocalName().equals("country") && !paisTrobat){
												paisTrobat = true;
												
											}  
											else if(reader.getLocalName().equals("name") && paisTrobat){
												String pais=reader.getElementText().trim();
												
												if(pais.equals(pa)){
													trobat = true; 
													System.out
															.println("entraaaaaaaa paissss "+pais);
													pai = pais;
												} else{ 
													trobat = false;
													//paisTrobat = false; 
												}
											}  
	
											if(trobat){
												if(reader.getLocalName().equals("ethnicgroups") ){
													grups.add(reader.getElementText());
													System.out
															.println("etnics");
												} 
												else if(reader.getLocalName().equals("border")){
													borders += Double.parseDouble(reader.getAttributeValue(1));
													System.out
															.println("border: "+borders);
												}
											}
										break; 
										
										case XMLStreamReader.END_ELEMENT:
											if(reader.getLocalName().equals("country") ){
												p=new Pais(pai, borders, grups);
												paisTrobat=false;
												System.out.println("part 3" + p.toString());
											}
											
										break; 
										default:							
												break;
									}
								} 
	
							} catch (XMLStreamException e) {
								e.printStackTrace();
							}
						} 
						
					}
					temps = System.currentTimeMillis()-temps; 
					total3+=temps;
					if(paissos.size() == 4) quatre3=total3;
			} catch (XQException e) { 
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			        }
			       
			    
			 } catch ( IOException e1) {
				 // TODO Auto-generated catch block
				 e1.printStackTrace();
			 }
		} catch (FileNotFoundException e1) {
			 // TODO Auto-generated catch block
			 e1.printStackTrace();
		} finally {
	        try {
				br.close(); 
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
		try {
			new Close().execute(server.context);
			server.stop();
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Amb 4 països: \nel primer:"+quatre1+" milisegons\nel segon:"+quatre2+" milisegons\nel tercer:"+quatre3); 
		
		 
		System.out.println("Amb "+paissos.size()+" païssos:\nel 1 tarda "+total1+" milisegons.");
		System.out.println("el 2 tarda "+total2+" milisegons.");
		System.out.println("el 3 tarda "+total3+" milisegons.");
		 
		System.out.println("---AMB 4 PAÏSSOS---");
		if(quatre1<quatre2 && quatre1<quatre3) System.out.println("El primer és més òptim");
		else if(quatre2<quatre1 && quatre2<quatre3) System.out.println("El segon és més òptim");
		else System.out.println("El tercer és més òptim");
		System.out.println("el tercer és "+(quatre1/quatre3)+" cops més ràpid que el primer.");
		
		System.out.println("---AMB "+paissos.size()+" païssos---");
		if(total1<total2 && total1<total3) System.out.println("El primer és més òptim");
		else if(total2<total1 && total2<total3) System.out.println("El segon és més òptim");
		else System.out.println("El tercer és més òptim");
		System.out.println("el tercer és "+(total1/total3)+" cops més ràpid que el primer.");
		
	}

}